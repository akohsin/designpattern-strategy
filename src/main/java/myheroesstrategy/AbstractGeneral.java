package myheroesstrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AbstractGeneral implements IGeneralStrategy {
    List<AbstractSoldiers> army = new ArrayList<AbstractSoldiers>();

    public AbstractGeneral(int experiencePoints) {
        this.army = generateSoldier(experiencePoints);
    }

    public List<AbstractSoldiers> generateSoldier(int experiencePoints) {
        int counter = experiencePoints;
        Random rand = new Random(1);
        List<AbstractSoldiers> army = new ArrayList<AbstractSoldiers>();
        while (counter > 0) {
            int tmp = rand.nextInt(101);
            switch (rand.nextInt(3)) {
                case 0: {
                    army.add(new ArcherSoldier(tmp));
                    break;
                }
                case 1: {
                    army.add(new KnightSoldier(tmp));
                    break;
                }
                case 2: {
                    army.add(new MageSoldier(tmp));
                    break;
                }
            }
        }
        return army;
    }

    public void removeSoldier(int index) {
        army.remove(index);
    }

    public AbstractSoldiers getSoldier(int index) {
        return army.get(index);
    }

    public int sizeOfArmy() {
        return army.size();
    }

    public void fight(IGeneralStrategy nextGeneral) {
        for (AbstractSoldiers soldier : army) {
            int index = new Random().nextInt(nextGeneral.sizeOfArmy());
            if (soldier.fight(nextGeneral.getSoldier(index))){
                nextGeneral.removeSoldier(index);
            }
        }
    }
}
