package myheroesstrategy;

public class Arena {
    LeftSideGeneral firstPlayer = new LeftSideGeneral();
    RightSideGeneral secondPlayer = new RightSideGeneral();
    boolean isWinner = false;

    public void war() {
        boolean side = true;
        while (!isWinner) {
            System.out.println("Round : size of 1st players army: " +
                    firstPlayer.sizeOfArmy() + " , size of 2nd players army: " + secondPlayer.sizeOfArmy());
            if (side) {
                firstPlayer.fight(secondPlayer);
            } else {
                secondPlayer.fight(secondPlayer);
            }
            if (secondPlayer.sizeOfArmy()==0){
                System.out.println("the winner is .. first player");
                isWinner=true;
            }
            else if (firstPlayer.sizeOfArmy()==0){
                System.out.println("the winner is .. second player");
                isWinner=true;
            }
            side = !side;

        }
    }

//    4. Stwórz klasę Arena która w konstruktorze tworzy 1 generała z jednej strony, a następnie 1 generała z 2 strony.
//    ma metodę walki:
//    public void war() - która w pętli wykonuje atak jednego generała na drugim, a następnie w drugą stronę (po 1 turze).
//
//    UWAGA! rozpocznij od metod generowania wojska. Osoba w grupie powinna zaimplementować jednego generała. Po wykonaniu
//    całego zadania porównajcie wojska
}
