package myheroesstrategy;

public class AbstractSoldiers {
    int exp = 0;
    int health = 1000;
    int defArch = 0;
    int defH2H = 0;
    int defMgc = 0;

    public AbstractSoldiers(int exp, int defArch, int defH2H, int defMgc) {
        this.exp = exp;
        this.defArch = defArch;
        this.defH2H = defH2H;
        this.defMgc = defMgc;
    }

    public int getExp() {
        return exp;
    }

    public boolean subHealth(int x) {
        if (x >= this.health) {
            this.health = 0;
            return true;
        }
        this.health -= x;
        return false;
    }

    public boolean fight(AbstractSoldiers soldierToFightWith) {
        int lostHealth = 0;
        if (soldierToFightWith instanceof MageSoldier) {
            lostHealth = exp / 10 - defMgc * 2 * soldierToFightWith.getExp();
        }
        if (soldierToFightWith instanceof ArcherSoldier) {
            lostHealth = exp / 10 - defArch * 2 * soldierToFightWith.getExp();
        }
        if (soldierToFightWith instanceof KnightSoldier) {
            lostHealth = exp / 10 - defH2H * 2 * soldierToFightWith.getExp();
        }
        if (lostHealth < 0) lostHealth = 0;
        else if (lostHealth < 5) lostHealth = 5;
        return soldierToFightWith.subHealth(lostHealth);

    }


}
