package myheroesstrategy;

import java.util.List;

public interface IGeneralStrategy {
    List<AbstractSoldiers> generateSoldier(int experiencePoints);
     AbstractSoldiers getSoldier(int index);
    int sizeOfArmy();
    void removeSoldier(int index);
}
