package myreader.strategies;

import java.util.Random;

/**
 *
 * Created by amen on 8/17/17.
 */
public class RandomStrategy implements IInputStrategy {
    Random rand;
    String randomString;
    int randomInt;
    double randomDouble;
    public RandomStrategy() {
    rand = new Random();
    }

    public int getInt() {
        return rand.nextInt();
    }

    public String getString() {
        StringBuilder tmp = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            tmp.append((char)rand.nextInt(126));
        }

        return tmp.toString();
    }

    public double getDouble() {
        return rand.nextDouble();
    }
}
