package myreader.strategies;

import myreader.strategies.IInputStrategy;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by amen on 8/17/17.
 */
public class FileStrategy implements IInputStrategy {
    String sciezka = "File.txt";
    Scanner sc;

    public int getInt() {
        return sc.nextInt();
    }

    public String getString() {
        return sc.next();
    }

    public double getDouble() {
        return sc.nextDouble();
    }

    public FileStrategy() {
        try {
            this.sc = new Scanner(new File(sciezka));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


}
